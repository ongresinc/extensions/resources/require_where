#include "postgres.h"
#include "fmgr.h"
#include "parser/analyze.h"
#include "utils/elog.h"
#include "utils/guc.h"
#include "utils/errcodes.h"

#if PG_VERSION_NUM > 140000
#include "utils/queryjumble.h"
#endif

PG_MODULE_MAGIC;

void    _PG_init(void);
void    _PG_fini(void);

static  post_parse_analyze_hook_type prev_post_parse_analyze_hook = NULL;
