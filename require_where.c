/*
    require_where
    Based on David Fetters's patch. Updated withe the latest changes on the hook.
 */

#include "require_where.h"

static bool requireWhereEnabled = false;

static void
require_where_check(ParseState *pstate, 
                    Query *query
#if PG_VERSION_NUM > 140000
					,JumbleState *jstate
#endif
)
{
    if (!requireWhereEnabled)
        goto skipcheck;

    switch (query->commandType)
    {
    case CMD_DELETE:
		/* Make sure there's something to look at. */
		Assert(query->jointree != NULL);
		if (query->jointree->quals == NULL)
			ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("DELETE requires a WHERE clause when require_where.enable is set to on"),
					 errhint("To delete all rows, use \"WHERE true\" or similar.")));

        break;
    case CMD_UPDATE:
		Assert(query->jointree != NULL);
		if (query->jointree->quals == NULL)
			ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("UPDATE requires a WHERE clause when require_where.enable is set to on"),
					 errhint("To update all rows, use \"WHERE true\" or similar.")));

        break;
    default:
        break;
    }

skipcheck:

#if PG_VERSION_NUM > 140000
    if (compute_query_id)
        jstate = JumbleQuery(query,pstate->p_sourcetext );
#endif

	if (prev_post_parse_analyze_hook != NULL)
		(*prev_post_parse_analyze_hook) 
            (pstate, query
#if PG_VERSION_NUM > 140000     // See https://github.com/postgres/postgres/commit/5fd9dfa5f50e4906c35133a414ebec5b6d518493
            ,jstate
#endif
            );
}

void _PG_init(void)
{
    DefineCustomBoolVariable("require_where.enabled",
						 "Enable require_where filter check",
						 NULL,
						 &requireWhereEnabled,
						 false,
						 PGC_SUSET,
						 0,
						 NULL,
						 NULL,
						 NULL);
	prev_post_parse_analyze_hook = post_parse_analyze_hook;
	post_parse_analyze_hook = require_where_check;
}

void _PG_fini(void)
{
	post_parse_analyze_hook = prev_post_parse_analyze_hook;
}
