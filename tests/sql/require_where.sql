CREATE TABLE testtable AS SELECT i, random()::text FROM generate_series(1,10) i(i);
LOAD 'require_where';
SET require_where.enabled = on;
DELETE FROM testtable;
SET require_where.enabled = off;
DELETE FROM testtable;