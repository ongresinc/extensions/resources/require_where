# require_where

Adaptation of [this patch](https://www.postgresql.org/message-id/attachment/94301/training_wheels_007.patch) from David Fetter for exampling about parser hooks.

This one, uses the `post_parse_analyze_hook_type` for intecepting DELETE and UPDATE statements that do not have filtering.

It has been updated to the last Postgres version, in which the hook
have changed due to pg_stat_statements brought into the core, adding an additional argument (Jumble State).

This extension does not detect tautologies, we'll probably need to examine node tag `query->jointree->quals`/